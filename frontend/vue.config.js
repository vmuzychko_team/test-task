module.exports = {
    outputDir: 'target/ui',
    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'i18n',
            enableInSFC: false
        }
    },
    devServer: {
        port: 9000,
        proxy: 'http://localhost:8080/testtask',
    }
};
