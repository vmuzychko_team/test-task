package net.vmuzychko.testtask.controller;

import com.google.common.base.Preconditions;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by Viktoria Muzychko on 21.09.2019.
 */
public class CreateTransactionDto implements Serializable {
    @NotNull
    private Integer amount;
    @NotNull
    private TransactionType type;

    CreateTransactionDto() {
    }

    public CreateTransactionDto(TransactionType type, Integer amount) {
        this.amount = amount;
        Preconditions.checkArgument(this.amount > 0, "Amount must be a positive value greater than 0!");
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public TransactionType getType() {
        return type;
    }

    public int getAbsoluteAmount() {
        return type == TransactionType.CREDIT ? amount : amount * (-1);
    }
}
