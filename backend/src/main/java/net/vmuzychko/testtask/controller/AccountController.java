package net.vmuzychko.testtask.controller;

import net.vmuzychko.testtask.service.TransactionService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AccountController {

    private final TransactionService transactionService;

    public AccountController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping()
    public String getBalance() {
        return "Balance: " + transactionService.getBalance();
    }
}
