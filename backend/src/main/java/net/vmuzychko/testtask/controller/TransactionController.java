package net.vmuzychko.testtask.controller;

import net.vmuzychko.testtask.service.TransactionService;
import net.vmuzychko.testtask.storage.BalanceAmountException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static net.vmuzychko.testtask.controller.Api.TRANSACTIONS;

@RestController
@RequestMapping(TRANSACTIONS)
public class TransactionController {

    private final TransactionService transactionService;

    public TransactionController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping
    public List<TransactionInfoDto> getHistory() {
        return transactionService.getTransactions();
    }

    @GetMapping("/{id}")
    public TransactionInfoDto getById(@PathVariable String id) {
        return transactionService.getById(id);
    }

    @PostMapping
    public ResponseEntity<TransactionInfoDto> addNewTransaction(@RequestBody @Valid CreateTransactionDto createTransactionDto) {
        try {
            return ResponseEntity.status(HttpStatus.OK)
                    .body(transactionService.addTransaction(createTransactionDto));
        } catch (BalanceAmountException e) {
            // todo handle properly
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST).build();
        }
    }
}
