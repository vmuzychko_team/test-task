package net.vmuzychko.testtask.controller;

public final class Api {

    public static final String API_PREFIX = "/api";
    public static final String TRANSACTIONS = API_PREFIX + "/transactions";

    private Api() {
    }
}