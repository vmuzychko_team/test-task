package net.vmuzychko.testtask.controller;

public enum TransactionType {
    DEBIT, CREDIT
}
