package net.vmuzychko.testtask.controller;


import java.io.Serializable;

public class TransactionInfoDto implements Serializable {
    private String id;
    private TransactionType type;
    private int amount;
    private String effectiveDate;

    TransactionInfoDto() {
    }

    public String getId() {
        return id;
    }

    public TransactionType getType() {
        return type;
    }

    public int getAmount() {
        return amount;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    private TransactionInfoDto(Builder builder) {
        id = builder.id;
        type = builder.type;
        amount = builder.amount;
        effectiveDate = builder.effectiveDate;
    }

    public static class Builder {
        private String id;
        private TransactionType type;
        private int amount;
        private String effectiveDate;

        public Builder withId(String id) {
            this.id = id;
            return this;
        }

        public Builder withType(TransactionType type) {
            this.type = type;
            return this;
        }

        public Builder withAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder withEffectiveDate(String effectiveDate) {
            this.effectiveDate = effectiveDate;
            return this;
        }

        public TransactionInfoDto build() {
            return new TransactionInfoDto(this);
        }
    }
}
