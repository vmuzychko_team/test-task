package net.vmuzychko.testtask.service;


import net.vmuzychko.testtask.controller.TransactionInfoDto;
import net.vmuzychko.testtask.storage.Transaction;

import java.time.format.DateTimeFormatter;

public class TransactionMapper {
    private TransactionMapper() {
    }

    public static TransactionInfoDto mapToDto(Transaction transaction) {
        return new TransactionInfoDto.Builder()
                .withType(transaction.getType())
                .withAmount(transaction.getAmount())
                .withEffectiveDate(DateTimeFormatter.ofPattern("dd.MM.uuuu").format(transaction.getEffectiveDate()))
                .withId(transaction.getId())
                .build();
    }
}
