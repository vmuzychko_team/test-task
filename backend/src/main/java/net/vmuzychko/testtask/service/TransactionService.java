package net.vmuzychko.testtask.service;


import net.vmuzychko.testtask.controller.CreateTransactionDto;
import net.vmuzychko.testtask.controller.TransactionInfoDto;
import net.vmuzychko.testtask.storage.BalanceAmountException;
import net.vmuzychko.testtask.storage.ConcurrentStorage;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private final ConcurrentStorage storage;

    public TransactionService(ConcurrentStorage storage) {
        this.storage = storage;
    }

    public int getBalance() {
        return storage.getBalance();
    }

    public List<TransactionInfoDto> getTransactions() {
        return storage.getTransactions().stream().map(TransactionMapper::mapToDto).collect(Collectors.toList());
    }

    public TransactionInfoDto addTransaction(CreateTransactionDto createTransactionDto) throws BalanceAmountException {
        return TransactionMapper.mapToDto(storage.addTransaction(createTransactionDto));
    }

    public TransactionInfoDto getById(String id) {
        return TransactionMapper.mapToDto(storage.getById(id));
    }
}
