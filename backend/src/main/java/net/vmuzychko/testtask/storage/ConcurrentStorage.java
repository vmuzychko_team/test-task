package net.vmuzychko.testtask.storage;

import net.vmuzychko.testtask.controller.CreateTransactionDto;
import net.vmuzychko.testtask.controller.TransactionType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Service
public class ConcurrentStorage {

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final Lock readLock = lock.readLock();
    private final Lock writeLock = lock.writeLock();

    // todo: rework to map
    private List<Transaction> transactions = new ArrayList<>();

    public int getBalance() {
        readLock.lock();
        try {
            return getCurrentBalance();
        } finally {
            readLock.unlock();
        }
    }

    public List<Transaction> getTransactions() {
        readLock.lock();
        try {
            return transactions;
        } finally {
            readLock.unlock();
        }
    }

    public Transaction getById(String id) {
        // todo rework to map
        return transactions.stream()
                .filter(tr -> tr.getId().equals(id))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("No transaction with such id"));
    }

    public Transaction addTransaction(CreateTransactionDto createTransactionDto) throws BalanceAmountException {
        writeLock.lock();
        try {
            if (getCurrentBalance() + createTransactionDto.getAbsoluteAmount() < 0) {
                throw new BalanceAmountException("Current transaction rejected, insufficient balance!");
            }
            final Transaction transaction = new Transaction(createTransactionDto.getType(), createTransactionDto.getAmount());
            transactions.add(transaction);
            return transaction;
        } finally {
            writeLock.unlock();
        }
    }

    private int getCurrentBalance() {
        final Integer debitValue = transactions.stream()
                .filter(tr -> tr.getType() == TransactionType.DEBIT)
                .map(Transaction::getAmount).reduce(0, Integer::sum);
        final Integer creditValue = transactions.stream()
                .filter(tr -> tr.getType() == TransactionType.CREDIT)
                .map(Transaction::getAmount).reduce(0, Integer::sum);
        return creditValue - debitValue;
    }
}
