package net.vmuzychko.testtask.storage;


import net.vmuzychko.testtask.controller.TransactionType;

import java.time.LocalDate;
import java.util.Objects;
import java.util.UUID;

public class Transaction {
    private String id;
    private TransactionType type;
    private int amount;
    private LocalDate effectiveDate;

    public Transaction(TransactionType type, int amount) {
        this.type = type;
        this.amount = amount;
        this.effectiveDate = LocalDate.now();
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public TransactionType getType() {
        return type;
    }

    public int getAmount() {
        return amount;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return amount == that.amount &&
                Objects.equals(id, that.id) &&
                type == that.type &&
                Objects.equals(effectiveDate, that.effectiveDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, type, amount, effectiveDate);
    }
}
