package net.vmuzychko.testtask.storage;


public class BalanceAmountException extends Exception {
    public BalanceAmountException(String message) {
        super(message);
    }
}
