package net.vmuzychko.testtask.controller;

import org.junit.Test;

import static org.junit.Assert.*;

public class CreateTransactionDtoTest {

    @Test
    public void shouldReturnCorrectAbsoluteAmountValue() {
        assertTrue(new CreateTransactionDto(TransactionType.CREDIT, 90).getAbsoluteAmount() == 90);
        assertTrue(new CreateTransactionDto(TransactionType.DEBIT, 50).getAbsoluteAmount() == -50);
    }
}