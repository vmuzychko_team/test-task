package net.vmuzychko.testtask.storage;

import net.vmuzychko.testtask.controller.CreateTransactionDto;
import net.vmuzychko.testtask.controller.TransactionType;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;


public class ConcurrentStorageTest {

    private ConcurrentStorage concurrentStorage;

    @Before
    public void setUp() {
        concurrentStorage = new ConcurrentStorage();
    }

    @Test
    public void shouldStoreAllTransactions() throws Exception {
        final Transaction firstTransaction = concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.CREDIT, 100));
        final Transaction secondTransaction = concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.DEBIT, 10));
        final Transaction thirdTransaction = concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.DEBIT, 20));

        final List<Transaction> transactions = concurrentStorage.getTransactions();
        assertTrue(transactions.size() == 3);
        assertTrue(transactions.contains(firstTransaction));
        assertTrue(transactions.contains(secondTransaction));
        assertTrue(transactions.contains(thirdTransaction));
    }


    @Test
    public void shouldGetCorrectBalance() throws Exception {
        concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.CREDIT, 100));
        concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.DEBIT, 10));
        assertTrue(concurrentStorage.getBalance() == 90);

        concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.DEBIT, 90));
        assertTrue(concurrentStorage.getBalance() == 0);
    }

    @Test(expected = BalanceAmountException.class)
    public void shouldNotAllowNegativeBalance() throws Exception {
        concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.CREDIT, 100));
        concurrentStorage.addTransaction(new CreateTransactionDto(TransactionType.DEBIT, 101));
    }

    @Test
    public void shouldHaveZeroBalanceAfterInstantiation() throws Exception {
        assertTrue(concurrentStorage.getBalance() == 0);
    }
}