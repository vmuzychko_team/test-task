I completed backend part and unfortunately don't have spare time anymore to complete frontend module.


# Backend

This is a Spring Boot application.

## Build

You can build the application with Maven:
```
mvn clean verify
```


## Run

You can run the application with Maven.
```
mvn spring-boot:run
```

# Frontend

## Setup
```
npm ci
```

## Run
```
npm run serve
```